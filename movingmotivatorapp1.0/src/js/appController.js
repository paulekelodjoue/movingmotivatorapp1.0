/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 * @ignore
 */
/*
 * Your application specific code will go here
 */
define(['ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils', 'knockout', 'ojs/ojknockout'
        , 'knockout', 'ojs/ojknockout','ojs/ojinputtext', 'ojs/ojcheckboxset', 'ojs/ojbutton'
        , 'ojs/ojformlayout', 'ojs/ojinputtext', 'ojs/ojvalidationgroup'
         //, 'services/firebase'
         //, 'services/firebase-app'
         
          ],
  function(ResponsiveUtils, ResponsiveKnockoutUtils, ko) {
     function ControllerViewModel() {
       var self = this;
       this.emailManager = ko.observable("");
       this.nameManager = ko.observable("");
       this.funktionManager = ko.observable("");
       this.emailMitglied = ko.observable("");
       this.nameMitglied = ko.observable("");
       this.funktionMitglied = ko.observable("");

      // Media queries for repsonsive layouts
      var smQuery = ResponsiveUtils.getFrameworkQuery(ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
      self.smScreen = ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);

      // Header
      // Application Name used in Branding Area
      self.appName = ko.observable("Moving Motivators");
      this.submit = function () {
        _compareFieldsAddMessagesCustom(this.email(), this.email2());
        var valid = _checkValidationGroup();
        if (valid) {
          // submit the form would go here
          // eslint-disable-next-line no-alert
          alert('everything is valid; submit the form');
        }
      }.bind(this);
      var _checkValidationGroup = function () {
        var tracker = document.getElementById("tracker");
        if (tracker.valid === "valid") {
          return true;
        }
      }
         /* else {
          // show messages on all the components
          // that have messages hidden.
          tracker.showMessages();
          tracker.focusOn("@firstInvalidShown");
          return false;
        }
      };
    }
     */
      // Footer
      function footerLink(name, id, linkTarget) {
        this.name = name;
        this.linkId = id;
        this.linkTarget = linkTarget;
      }
      self.footerLinks = ko.observableArray([
        new footerLink('AGB', 'agb', 'https://www.it-p.de/agb'),
        new footerLink('About IT-P', 'aboutItp', 'http://www.oracle.com/us/corporate/index.html#menu-about'),
        new footerLink('Datenschutz', 'datenschutz', 'https://www.it-p.de/datenschutz'),
        new footerLink('Kontakt', 'kontakt', 'https://www.it-p.de/kontakt'),
        new footerLink('Impressum', 'impressum', 'https://www.it-p.de/impressum')
      
      ]);
     }

     return new ControllerViewModel();
  }
);
